################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../ap.cpp \
../application.cpp \
../bridge.cpp \
../emu.cpp \
../generator.cpp \
../hub.cpp \
../network-hardware.cpp \
../node.cpp \
../ping.cpp \
../point-to-point.cpp \
../tap.cpp \
../tcp-large-transfer.cpp \
../udp-echo.cpp \
../utils.cpp 

OBJS += \
./ap.o \
./application.o \
./bridge.o \
./emu.o \
./generator.o \
./hub.o \
./network-hardware.o \
./node.o \
./ping.o \
./point-to-point.o \
./tap.o \
./tcp-large-transfer.o \
./udp-echo.o \
./utils.o 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Local XL C++ Compiler'
	/usr/vacpp/bin/xlC -c -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


