################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../examples/example-bridge.cpp \
../examples/example-hub.cpp \
../examples/example-p2p.cpp \
../examples/example-router-bridge.cpp \
../examples/example-router.cpp \
../examples/example-star.cpp \
../examples/example-tcpLargeTransfer.cpp \
../examples/example-udpEcho.cpp \
../examples/example-wifi-ap.cpp 

OBJS += \
./examples/example-bridge.o \
./examples/example-hub.o \
./examples/example-p2p.o \
./examples/example-router-bridge.o \
./examples/example-router.o \
./examples/example-star.o \
./examples/example-tcpLargeTransfer.o \
./examples/example-udpEcho.o \
./examples/example-wifi-ap.o 


# Each subdirectory must supply rules for building sources it contributes
examples/%.o: ../examples/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Local XL C++ Compiler'
	/usr/vacpp/bin/xlC -c -g -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


