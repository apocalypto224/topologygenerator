/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <juan-@gitlab.com>
 */

/**
 * \file template.h
 * \brief The event class is subclass of Application.
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef TEMPLATE_H
#define TEMPLATE_H

#include "event.h"

#include <iostream>
#include <string>
#include <vector>

/**
 * \ingroup generator
 * \brief CyberVan Template class
 *
 *  The template class represent the cybervan xml templates.
 *  The class specifies :
 *    - name
 *    - indice
 *    - template
 *    - template type
 *    - number of CPUs
 *    - amount of memory
 *    - ipmask
 *    - events
 *    - gui_properties
 *
 */

class Template
{
  private:
    /**
     * \brief Application Script.
     */
    std::string m_name;

    /**
     * \brief Indice of template
     */
    size_t m_indice;

    /**
     * \brief Base Image
     */
    std::string m_img;

    /**
     * \brief Base Image Type
     */
    std::string m_imgType;

    /**
     * \brief Number of CPUs per Node
     */
    size_t m_cpu;

    /**
     * \brief Amount of memory per Node
     */
    size_t m_mem;

    /**
     * \brief IP Mask. Default 255.255.255.0
     */
    size_t m_ipmask;

    /**
     * \brief Determines whether script is run at stop time
     */
    std::vector<Event*> m_events;
    
    /**
     * \brief number of events in template
     */
    size_t m_indiceEvent;

  public:
    /**
     * \brief Constructor.
     * \param name template name
     * \param indice indice in the generator vector
     * \param img base image
     * \param imgType base image type
     * \param cpu number of CPUs
     * \param mem amount of memory
     * \param ipmask ip mask
     */
    Template(const std::string &name, const size_t &indice, const std::string &img, const std::string &imgType, const size_t &cpu, const size_t &mem, const size_t &ipmask);

    /**
     * \brief Destructor.
     */
    ~Template();

    /**
     * \brief Get Template number.
     * \return template number
     */
    std::string GetIndice();

    /**
     * \brief Set template number.
     * \param indice new indice
     */
    void SetIndice(const size_t &indice);

    /**
     * \brief Get template name.
     * \return template name
     */
    std::string GetTempName();

    /**
     * \brief Set template name.
     * \param name new template name
     */
    void SetTempName(const std::string &name);
    
    /**
     * \brief Get template base image
     * \return template base image
     */
    std::string GetImg();

    /**
     * \brief Set template base image
     * \param img new base image
     */
    void SetImg(const std::string &img);
    
    /**
     * \brief Get template base image type
     * \return template image type
     */
    std::string GetImgType();

    /**
     * \brief Set template image type
     * \param imgType new template base image type pvm or hvm
     */
    void SetImgType(const std::string &imgType);
    
    /**
     * \brief Get number of CPUs
     * \return number of cpus as string
     */
    std::string GetCPU();

    /**
     * \brief Set number of CPUs
     * \param cpu number of CPUs
     */
    void SetCPU(const size_t &cpu);
    
    /**
     * \brief Get template memory amount
     * \return amount of memory as string
     */
    std::string GetMem();

    /**
     * \brief Set template memory amount
     * \param mem amount of memory
     */
    void SetMem(const size_t &mem);
    
    /**
     * \brief Get template IP mask.
     * \return template IP mask
     */
    std::string GetIPMask();

    /**
     * \brief Set template IP mask.
     * \param ipmask new template ip mask
     */
    void SetIPMask(const size_t &ipmask);

    /**
     * \brief Get Number of events in template
     * \return number of events
     */
    size_t GetNEvent();

    /**
     * \brief Get template event at a specific index
     * \param index index of desired event
     * \return Event object
     */
    Event* GetEvent(size_t index);

    /**
     * \brief Create a new template event
     * \param scriptName name of script to run
     * \param startTime time to start event (-1 for stop, 0 for start, or other)
     */
    void NewEvent(const std::string &scriptName, const int &startTime);

    /**
     * \brief Delete a template event
     * \param index event index
     */
    void DeleteEvent(const size_t &index);

    /**
     * \brief Generate template XML code 
     * \return vector with generated code
     */
    std::vector<std::string> GenerateTemplateCyberVan();

};

#endif /* TEMPLATE_H */

