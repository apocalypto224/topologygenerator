/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file node.h
 * \brief Node base class.
 * \author Pierre Weiss
 * \date 2009
 */

#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <string>
#include <vector>
#include "route.h"

/**
 * \ingroup generator
 * \brief Node base class.
 *
 *  The node class represent the nodes.
 *  You can create here différents node type :
 *    - pc
 *    - router
 *    - ap
 *    - station (wifi)
 *    - bridge
 *    - tap
 *    - emu
 *
 *  If you see node named as links, it is totaly normal.
 *  The different links need sometimes an node to be created in ns3.
 */
class Node 
{
  private:
    /**
     * \brief Node number.
     */
    size_t m_indice;

    /**
     * \brief Node name.
     */
    std::string m_nodeName;

    /**
     * \brief Ip interface name. 
     */
    std::string m_ipInterfaceName;

    /**
     * \brief Number of used IP interfaces
     */
    size_t m_ipInterfaceNumber;

    /**
     * \brief Nsc use.
     */
    std::string m_nsc;

    /**
     * \brief CyberVan Template use.
     */
    std::string m_template;

    /**
     * \brief CyberVan Base Image use.
     */
    std::string m_img;

    /**
     * \brief Base Image Type
     */
    std::string m_imgType;

    /**
     * \brief CyberVan Mac Address use.
     */
    std::string m_mac;

    /**
     * \brief Mac addresses of each interface for CyberVAN use.
     */
    std::vector<std::string> m_macStack;

    /**
     * \brief IP addresses of each interface for NS3 use.
     */
    std::vector<std::string> m_ipStack;

    /**
     * \brief mask of each interface for NS3 use.
     */
    std::vector<std::string> m_maskStack;

    /**
     * \brief Layer 2 interface flags.
     */
    bool * m_l2flags;

    /**
     * \brief Number of Node CPUs
     */
    size_t m_cpu;

    /**
     * \brief Amount of Node memory
     */
    size_t m_mem;

    /**
     * \brief Number of machines.
     */
    size_t m_machinesNumber;

    /**
     * \brief Maximum number of Interfaces
     */
    size_t IFACE_NUMBER;

    /**
     * \brief Node type.
     */
    std::string m_type;

    /**
     * \brief List of Routing rules
     * 
     * This attribute is the list of the created instance of route.
     */
    std::vector<Route*> m_listRoute;

    /**
     * \brief Number attribute of routes created
     */
    size_t m_indiceRoute;

  public:
    /**
     * \brief Constructor.
     * \param indice number of the node
     * \param type type of the node (pc, router, ...)
     * \param namePrefix the prefix name of the node
     * \param machinesNumber number of machine to create
     */
    Node(const size_t &indice, const std::string &type, const std::string &namePrefix, const size_t &machinesNumber);

    /**
     * \brief Destructor.
     */
    ~Node();

    /**
     * \brief Generate headers code.
     * This function return a vector which contain the header lines from the specified object
     * \return headers code
     */
    std::vector<std::string> GenerateHeader();

    /**
     * \brief Generate node C++ code.
     * This function return a vector which contain the declaration and instanciation of the node
     * \return node code
     */
    std::vector<std::string> GenerateNodeCpp();
    
    /**
     * \brief Generate node CyberVan XML code.
     * This function return a vector which contain the header and image tags of the node
     * \return node code
     */
    std::vector<std::string> GenerateNodeCyberVan();

    /**
     * \brief Generate IP stack C++ code.
     * This function return a vector which contain the c++ code from the Ipv4 stack declaration and instanciation
     * \return IP stack code.
     */
    std::vector<std::string> GenerateIpStackCpp();

    /**
     * \brief Generate node python code.
     * This function return a vector which contain the declaration and instanciation of the node
     * \return node code
     */
    std::vector<std::string> GenerateNodePython();

    /**
     * \brief Generate IP stack python code.
     * This function return a vector which contain the c++ code from the Ipv4 stack declaration and instanciation
     * \return IP stack code.
     */
    std::vector<std::string> GenerateIpStackPython();

    /**
     * \brief Set node name.
     * \param nodeName node name
     */
    void SetNodeName(const std::string &nodeName);

    /**
     * \brief Set ip interface name.
     *
     * This procedure is used to the the ipInterfaceName.
     * Sometimes this var is used in application like as UdpEcho.
     * \param ipInterfaceName ip interface name
     */
    void SetIpInterfaceName(const std::string &ipInterfaceName);

    /**
     * \brief Set ip interface number.
     *
     * This procedure is used to loop through the flag array more
     * efficiently
     * \param number number of Ip interfaces of the node
     */
    void SetIpInterfaceNumber(const size_t &number);

    /**
     * \brief Get node name.
     * \return node name
     */
    std::string GetNodeName();

    /**
     * \brief Get node name.
     * \param number machine number
     * \return node name
     */
    std::string GetNodeName(const size_t &number);

    /**
     * \brief Get ip interface name.
     * \return IP interface name
     */
    std::string GetIpInterfaceName();

    /**
     * \brief Get number of ip interfaces.
     * \return IP interface number
     */
    size_t GetIpInterfaceNumber();

    /**
     * \brief Set Layer 2 IP interface flag
     * \param index interface index
     */
    void SetL2Flag(const size_t &index);

    /**
     * \brief Unset Layer 2 IP interface flag
     * \param index interface index
     */
    void UnsetL2Flag(const size_t &index);

    /**
     * \brief Check if Layer 2 IP interface flag is set
     * \param index interface index
     * \return true if flag is set
     */
    bool IsSetL2Flag(const size_t &index);

    /**
     * \brief Get indice.
     * \return indice
     */
    std::string GetIndice();

    /**
     * \brief Set indice.
     * \param indice new indice
     */
    void SetIndice(const size_t &indice);

    /**
     * \brief Get NSC (Network Simulation Cradle) code.
     * \return nsc code
     */
    std::string GetNsc();

    /**
     * \brief Set NSC (Network Simulation Cradle) code.
     * \param nsc new nsc code
     */
    void SetNsc(const std::string &nsc);

    /**
     * \brief Get Template name for CyberVan XML file.
     * \return template name
     */
    std::string GetTemplate();

    /**
     * \brief Set Template name for CyberVan XML file.
     * \param template set template name
     */
    void SetTemplate(const std::string &tem);

    /**
     * \brief Get Base Image name for CyberVan XML file.
     * \return Base Image name
     */
    std::string GetImg();

    /**
     * \brief Set Base Image name for CyberVan XML file.
     * \param img set base image name
     */
    void SetImg(const std::string &img);

        /**
     * \brief Get Base Image type for CyberVan XML file.
     * \return Base Image type
     */
    std::string GetImgType();

    /**
     * \brief Set Base Image type for CyberVan XML file.
     * \param img set base image type
     */
    void SetImgType(const std::string &imgType);

    /**
     * \brief Get Mac address for CyberVan XML file.
     * \return Mac address
     */
    std::string GetMac(const size_t &index);

    /**
     * \brief Set Mac Address for CyberVan XML file.
     * \param index index of interface
     * \param mac set mac address
     */
    void SetMac(const size_t &index, const std::string &mac);

    /**
     * \brief Get IP address of interface.
     * \return IP address
     */
    std::string GetIP(const size_t &index);

    /**
     * \brief Set IP address of specified interface
     * \param index index of interface
     * \param ip new IP address
     */
    void SetIP(const size_t &index, const std::string &ip);

    /**
     * \brief Get Mask of interface.
     * \return Mask as integer
     */
    std::string GetMask(const size_t &index);

    /**
     * \brief Set Mask of specified interface
     * \param index index of interface
     * \param mask new mask as integer
     */
    void SetMask(const size_t &index, const size_t &mask);

    /**
     * \brief Get CPU number.
     * \return number of cpus
     */
    std::string GetCPU();

    /**
     * \brief Set number of CPUs.
     * \param cpu number of CPUs
     */
    void SetCPU(const size_t &cpu);

    /**
     * \brief Get amount of memory.
     * \return amount of memory in bytes
     */
    std::string GetMem();

    /**
     * \brief Set amount of memory.
     * \param mem amount of memory in bytes
     */
    void SetMem(const size_t &mem);

    /**
     * \brief Get machines number.
     * \return machines number
     */
    size_t GetMachinesNumber();

    /**
     * \brief Set machines number.
     * \param machinesNumber new machines number
     */
    void SetMachinesNumber(const size_t machinesNumber);

    /**
     * \brief Get node type.
     * \return type the node type
     */
    std::string GetNodeType();

    /**
     * \brief Add a Route.
     * 
     * This procedure is used to add a route. It adds the route to the
     * vector listRoute and increments the number of routes by 1.
     * 
     * \param dest the destination of the route
     * \param gateway the local gateway to use
     * \param iFaceIndex the interface to use
     */
    void AddRoute(const std::string &dest, const std::string &mask, const std::string &gateway, const int &iFaceIndex);

    /**
     * \brief Remove a Route element.
     * \param index Route index to remove
     */
    void RemoveRoute(const size_t &index);

    /**
     * \brief Get Route element at specified index.
     * \param index index
     * \return Route pointer
     */
    Route* GetRoute(const size_t &index);

    /**
     * \brief Get number of Routes.
     * \return number of Routes
     */
    size_t GetNRoutes() const; 

};

#endif /* NODE_H */

