/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file event.cpp
 * \brief Node Event Class.
 * \author Juan Sepulveda (git: juan-)
 * \date 2016
 */

#include "event.h"
#include "generator.h"

#include "utils.h"

Event::Event(const std::string &type, const size_t &indice, const std::string &senderNode, const std::string &scriptName, const int &startTime) : Application(type, indice, senderNode, "", (size_t) startTime, startTime)
{
  this->m_scriptName = scriptName;
  this->SetAppName(std::string("event_" + this->GetIndice()));
  this->m_endScript = false;
  if (startTime == -1) this->m_endScript = true;
}

Event::~Event()
{
}

std::vector<std::string> Event::GenerateHeader()
{
  std::vector<std::string> headers;

  return headers;
}

std::string Event::GetScript()
{
  return this->m_scriptName;
}

void Event::SetScript(const std::string &name)
{
  this->m_scriptName = name;
}

std::string Event::GetTime()
{
  if (this->m_endScript) return "-1";
  return this->GetStartTime();
}

void Event::SetTime(const int &startTime)
{
  if(startTime == -1)
  {
    this->m_endScript = true;
  }
  else
  {
    this->m_endScript = false;
    this->SetStartTime( (size_t) startTime );
  }
}

std::vector<std::string> Event::GenerateApplicationCyberVan()
{
  std::vector<std::string> apps;
  if (m_endScript)
    apps.push_back("      <event time=\"stop\" script=\""+ this->m_scriptName +"\"/>");
  else if (this->GetStartTime() == "0")
    apps.push_back("      <event time=\"start\" script=\""+ this->m_scriptName +"\"/>");
  else
    apps.push_back("      <event time=\""+ this->GetStartTime() +"\" script=\""+ this->m_scriptName +"\"/>");

  return apps;
}

std::vector<std::string> Event::GenerateApplicationCpp(std::string netDeviceContainer, size_t numberIntoNetDevice)
{
  std::vector<std::string> apps;
  return apps;
}

std::vector<std::string> Event::GenerateApplicationPython(std::string netDeviceContainer, size_t numberIntoNetDevice)
{
  std::vector<std::string> apps;
  return apps;
}
