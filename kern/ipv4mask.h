/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; tab-width:2; -*- */

#include <string>
#include <stdint.h>

class Ipv4Mask {
public:
  Ipv4Mask(std::string mask);
  Ipv4Mask(size_t mask);
  
  std::string GetStr();
  size_t GetInt();
  std::string Mask(std::string ip);
  std::string PartialMask(std::string ip);

private:
  uint32_t m_mask;
};

