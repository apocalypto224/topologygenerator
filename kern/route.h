/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file node.h
 * \brief Node base class.
 * \author Pierre Weiss
 * \date 2009
 */

#ifndef ROUTE_H
#define ROUTE_H

#include <iostream>
#include <string>
#include <vector>

/**
 * \ingroup generator
 * \brief Route base class.
 *
 *  The node class represents static route protocols
 *
 *  If you see node named as links, it is totaly normal.
 *  The different links need sometimes an node to be created in ns3.
 */
class Route 
{
  private:
    /**
     * \brief Original route index in Generator list
     */
    size_t m_rIndex;

    /**
     * \brief Interface index the route should be applied to
     */
    int m_iFace;

    /**
     * \brief Destination Mask as integer
     */
    std::string m_mask;

    /**
     * \brief Node name.
     */
    std::string m_dest;

    /**
     * \brief Node type.
     */
    std::string m_gateway;

  public:
    /**
     * \brief Constructor.
     * \param dest Destination IP address
     * \param gateway Local gateway to direct traffic to
     */
    Route(const size_t &index, const std::string &mask, const std::string &dest, const std::string &gateway);

    /**
     * \brief Constructor.
     * \param dest Destination IP address
     * \param iFaceIndex interface index to use as gateway
     */
    Route(const size_t &index, const std::string &mask, const std::string &dest, const int &iFaceIndex);

    /**
     * \brief Destructor.
     */
    ~Route();

    /**
     * \brief Get Base Image name for CyberVan XML file.
     * \return Base Image name
     */
    std::string GetDestination();

    /**
     * \brief Set Base Image name for CyberVan XML file.
     * \param img set base image name
     */
    void SetDestination(const std::string &dest);

    /**
     * \brief Get Mac address for CyberVan XML file.
     * \return Mac address
     */
    std::string GetGateway();

    /**
     * \brief Set Mac Address for CyberVan XML file.
     * \param mac set mac address
     */
    void SetGateway(const std::string &gateway);

    /**
     * \brief Get Mac address for CyberVan XML file.
     * \return Mac address
     */
    int GetIFace();

    /**
     * \brief Set Mac Address for CyberVan XML file.
     * \param mac set mac address
     */
    void SetIFace(const int &iFaceIndex);

    /**
     * \brief Get Route mask.
     * \return mask as string
     */
    std::string GetMask();

    /**
     * \brief Set mask
     * \param mask as string
     */
    void SetMask(const std::string &mask);

};

#endif /* NODE_H */

