/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; tab-width:2 -*- */
/*
 * Copyright (c) 2009 University of Strasbourg
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pierre Weiss <3weissp@gmail.com>
 */

/**
 * \file drag-object.h
 * \brief The dragged object file.
 * \author Pierre Weiss
 * \date 2009
 */

#include <QtGui>
#include <iostream>

#include "drag-object.h"
#include "drag-widget.h"
#include "main-window.h"

DragObject::DragObject(QWidget *parent) : QLabel(parent)
{
  this->m_name = "";
  this->m_type = "";
  this->m_tooltip = "";
  //this->m_mw = parent->GetMainWindow;
}

DragObject::~DragObject()
{
}

void DragObject::contextMenuEvent(QContextMenuEvent * event)
{
  if(this->m_type == "Pc")
  {
    QMenu menu(this);
    QAction *temp = new QAction("Template", this);
    menu.addAction(temp);
    connect(temp, SIGNAL(triggered()), this->m_mw, SLOT(Templates()));
    QAction *net = new QAction("Network", this);
    menu.addAction(net);
    connect(net, SIGNAL(triggered()), this->m_mw, SLOT(EditNetwork()));
    QAction *route = new QAction("Routes", this);
    menu.addAction(route);
    connect(route, SIGNAL(triggered()), this->m_mw, SLOT(EditRoutes()));
    QAction *prop = new QAction("Properties", this);
    menu.addAction(prop);
    connect(prop, SIGNAL(triggered()), this->m_mw, SLOT(EditSettings()));
    menu.exec(event->globalPos());
  }
  else if (this->m_type == "Router")
  {
    QMenu menu(this);
    QAction *route = new QAction("Routes", this);
    menu.addAction(route);
    connect(route, SIGNAL(triggered()), this->m_mw, SLOT(EditRoutes()));
    menu.exec(event->globalPos());
  }
  return;
}

void DragObject::SetName(const std::string &name)
{
  this->m_name = name;
}

std::string DragObject::GetName()
{
  return this->m_name;
}

void DragObject::SetType(const std::string &type)
{
  this->m_type = type;
}

std::string DragObject::GetType()
{
  return this->m_type;
}

void DragObject::Destroy()
{
  this->m_name = "deleted";
  this->destroy();
}

void DragObject::SetToolTipText(const QString &str)
{
  this->m_tooltip = str;
}

QString DragObject::GetToolTipText()
{
  return this->m_tooltip;
}

void DragObject::SetMainWindow(MainWindow *mw)
{
  this->m_mw = mw;
}

