/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file routing-dialog.h
 * \brief Routing Settings dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef ROUTING_DIALOG_H
#define ROUTING_DIALOG_H

#include <QDialog>
#include "drag-widget.h"
#include "simulation-pages.h"
#include "utils.h"
#include "node.h"

class QListWidget;
class QListWidgetItem;
class QStackedWidget;

/**
 * \brief Routing dialog window class
 */
class RoutingDialog : public QDialog
{
  Q_OBJECT

  public:
    /**
     * \brief Application dialog constructor
     * \param dw
     */
    RoutingDialog(DragWidget *dw);
    
    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;
    

  public slots:
    
    /**
     * \brief quit
     */
    void quit();

    /**
     * \brief save and quit
     */
    void save();
    
    /**
     * \brief add row in table for new Route
     */
    void addRow();

  private:
    
    /**
     * \brief list routes that apply to nodes specified
     */
    void popTable();
    
    /**
     * \brief the content widget list
     */
    QTableWidget *tableWidget;

    /**
     * \brief whether multiple nodes are selected
     */
    bool multiple;

    /**
     * \brief selected node
     */
    Node *node;

    /**
     * \brief indexes of the Routing hardware this node is installed on
     */
    std::vector<size_t> m_rIndex;
};

#endif /* ROUTING_DIALOG_H */
