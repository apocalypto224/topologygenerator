/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file template-dialog.h
 * \brief Template dialog window
 * \author Juan Sepulveda
 * \date 2016
 */

#ifndef TEMPLATE_DIALOG_H
#define TEMPLATE_DIALOG_H

#include <QDialog>
#include "drag-widget.h"
#include "simulation-pages.h"
#include "utils.h"

class QListWidget;
class QListWidgetItem;
class QStackedWidget;

/**
 * \brief Templates dialog window class
 */
class TemplateDialog : public QDialog
{
  Q_OBJECT

  public:
    /**
     * \brief Template dialog constructor
     * \param dw
     */
    TemplateDialog(DragWidget *dw);
    
    /**
     * \brief drag widget object
     */
    DragWidget *m_dw;
    
    /**
     * \brief template page object
     */
    TemplatePage *m_template;

  public slots:
    
    /**
     * \brief quit
     */
    void quit();

    /**
     * \brief save and quit
     */
    void save();

    /**
     * \brief switch to another template
     * \param current
     * \param previous
     */
    void changePage(QListWidgetItem *current, QListWidgetItem *previous);

    /**
     * \brief add new template
     */
    void newTemp();

    /**
     * \brief delete selected template
     */
    void delTemp();

  private:
    /**
     * \brief list templates
     */
    void createTempList();

    /**
     * \brief the content widget list
     */
    QListWidget *contentsWidget;
    
    /**
     * \brief the page widget list
     */
    QStackedWidget *pagesWidget;

    /**
     * \brief the events widget table list
     */
    QStackedWidget *eventsWidget;

};

#endif /* TEMPLATE_DIALOG_H */
