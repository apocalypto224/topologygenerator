/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file network-dialog.cpp
 * \brief Network settings window
 * \author Juan Sepulveda
 * \date 2016
 */

#include <QtGui>

#include "network-dialog.h"
#include "simulation-pages.h"
#include "main-window.h"
#include "utils.h"

NetworkDialog::NetworkDialog(DragWidget *dw)
{
  this->m_dw = dw;
  this->m_iFaceNum = 0;

  tableWidget = new QTableWidget(this);  

  size_t numNodes = this->m_dw->m_mw->GetGenerator()->GetNNodes();
  std::string m_nodeName = this->m_dw->GetSelected()->GetName();
  for(size_t i = 0; i < numNodes; i++)
  {
    if (m_nodeName == this->m_dw->m_mw->GetGenerator()->GetNode(i)->GetNodeName()) 
    {
      m_nodeIndex = i;
      break;
    }
  }

  QPushButton *closeButton = new QPushButton(tr("Close"));
  QPushButton *saveButton = new QPushButton(tr("Save"));

  listInterfaces();

  connect(closeButton, SIGNAL(clicked()), this, SLOT(quit()));
  connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));

  QHBoxLayout *horizontalLayout = new QHBoxLayout;
  horizontalLayout->addWidget(tableWidget);

  QHBoxLayout *buttonsLayout = new QHBoxLayout;
  buttonsLayout->addStretch(3);
  buttonsLayout->addWidget(saveButton);
  buttonsLayout->addWidget(closeButton,1);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addLayout(horizontalLayout);
  mainLayout->addStretch(1);
  mainLayout->addSpacing(12);
  mainLayout->addLayout(buttonsLayout);
  setLayout(mainLayout);

  setWindowTitle(tr("Network Settings Dialog"));
}

void NetworkDialog::quit()
{
  emit close();
}

void NetworkDialog::save()
{
  // Save Node Properties
  this->m_dw->m_mw->GetGenerator()->GetNode(m_nodeIndex)->SetIpInterfaceNumber(m_iFaceNum);

  // Save Node Flags
  for (size_t i=0; i < m_iFaceNum; i++) {
    std::string b1 = tableWidget->item(i,0)->text().toStdString();
    std::string b2 = tableWidget->item(i,1)->text().toStdString();
    std::string b3 = tableWidget->item(i,2)->text().toStdString();
    size_t ipmask = tableWidget->item(i,3)->text().toInt();

    if (ipmask > 32){
      QMessageBox::about(this, "Network", "Please enter an IP mask value between 0 and 32.");
      return;
    }
    if ((b1 != "" || b2 != "" || b3 != "") && 
        (utils::stringToInteger(b1) < 256) && (utils::stringToInteger(b2) < 256) && (utils::stringToInteger(b3) < 256))
    {
      std::string ipBase = b1 + "." + b2 + "." + b3;
      this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(m_hIndex.at(i))->SetIpBase(ipBase);
      this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(m_hIndex.at(i))->SetIPMask(ipmask);
    }
    else
    {
      QMessageBox::about(this, "Network", "Please enter valid values for each IPv4 byte. Make sure it is between 0 and 255");
      return;
    }
  }
  this->m_dw->UpdateToolTips();
  QMessageBox::about(this, "Network", "Network settings saved successfully");
  this->close();
}

void NetworkDialog::listInterfaces()
{
  QTableWidgetItem *newItem = new QTableWidgetItem(tr(""));
  std::string nodeName = this->m_dw->GetSelected()->GetName();
  std::vector<std::string> IPs;
  std::vector<std::string> ipMasks;
  std::vector<std::string> base1;
  std::vector<std::string> base2;
  std::vector<std::string> base3;
  for(size_t i = 0; i < this->m_dw->m_mw->GetGenerator()->GetNNetworkHardwares(); i++)
  {
    size_t nodeNumber = this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetInstalledNodes().size();
    for(size_t j = 0; j < nodeNumber; j++)
    {
      std::vector<std::string> installedNodes =  this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetInstalledNodes();
      if( installedNodes.at(j) == nodeName )
      {
        m_hIndex.push_back(i);
        ipMasks.push_back(this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetIPMask());
        if (this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetIpBase() != "")
        {
          std::string base = this->m_dw->m_mw->GetGenerator()->GetNetworkHardware(i)->GetIpBase();
          size_t sByte = base.find(".") + 1;
          size_t tByte = base.find(".", sByte) + 1;
          if ((j > 0) && (installedNodes.at(j-1).find("ap_") == 0))
          {  
            IPs.push_back(base + "." + utils::integerToString(j));
            m_iFaceNum++;
          }
          else 
          {
            if ((j > 1) && (installedNodes.at(j-2) == nodeName)) 
            {
              IPs.push_back(base + "." + utils::integerToString(j));
              base1.push_back(base.substr(0,sByte-1));
              base2.push_back(base.substr(sByte,tByte - sByte - 1));
              base3.push_back(base.substr(tByte,3));
              m_iFaceNum++;
            }
            IPs.push_back(base + "." + utils::integerToString(j + 1));
            m_iFaceNum++;
          }
          base1.push_back(base.substr(0,sByte-1));
          base2.push_back(base.substr(sByte,tByte - sByte - 1));
          base3.push_back(base.substr(tByte,3));
        }
        else
        {
          if ((j > 0) && (installedNodes.at(j-1).find("ap_") == 0))
          {  
            IPs.push_back("10.0." + utils::integerToString(i) + "." + utils::integerToString(j));
            m_iFaceNum++;
          }
          else 
          {
            if ((j > 1) && (installedNodes.at(j-2) == nodeName)) 
            {
              IPs.push_back("10.0." + utils::integerToString(i) + "." + utils::integerToString(j));
              base1.push_back("10");
              base2.push_back("0");
              base3.push_back(utils::integerToString(i));
              m_iFaceNum++;
            }
            IPs.push_back("10.0." + utils::integerToString(i) + "." + utils::integerToString(j + 1));
            m_iFaceNum++;
          }
          base1.push_back("10");
          base2.push_back("0");
          base3.push_back(utils::integerToString(i));
        }
      }
    }
  }

  tableWidget->setMinimumWidth(300);
  //tableWidget->setMaximumWidth(219);
  tableWidget->setColumnCount(4);
  tableWidget->setColumnWidth(0, 30);
  tableWidget->setColumnWidth(1, 30);
  tableWidget->setColumnWidth(2, 30);
  // tableWidget->setColumnWidth(3, 80);
  tableWidget->setColumnWidth(3, 40);
  tableWidget->setRowCount(m_iFaceNum);
  tableWidget->horizontalHeader()->setStretchLastSection(true);
  //tableWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch); 
  
  QStringList labels;
  labels << "B1" << "B2" << "B3" /*<< "Address"*/ << "Mask";
  tableWidget->setHorizontalHeaderLabels(labels);

  for (size_t i = 0; i < m_iFaceNum; i++) 
  {
    newItem = new QTableWidgetItem(tr(base1.at(i).c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(i, 0, newItem); 
    newItem = new QTableWidgetItem(tr(base2.at(i).c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(i, 1, newItem); 
    newItem = new QTableWidgetItem(tr(base3.at(i).c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(i, 2, newItem);
    // newItem = new QTableWidgetItem(tr(IPs.at(i).c_str()));
    // newItem->setTextAlignment(Qt::AlignCenter); 
    // tableWidget->setItem(i, 3, newItem);
    newItem = new QTableWidgetItem(tr(ipMasks.at(i).c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(i, 3, newItem); 
  }

}
