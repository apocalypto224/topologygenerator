/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; tab-width:2; -*- */
/*
 * Copyright (c) 2016 Applied Communication Sciences 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Juan Sepulveda <github: juan->
 */

/**
 * \file routing-dialog.cpp
 * \brief Route settings window
 * \author Juan Sepulveda
 * \date 2016
 */

#include <QtGui>

#include "routing-dialog.h"
#include "simulation-pages.h"
#include "main-window.h"
#include "utils.h"

RoutingDialog::RoutingDialog(DragWidget *dw)
{
  this->m_dw = dw;
  this->node = dynamic_cast<Node *>(this->m_dw->m_mw->GetGenerator()->GetNode(this->m_dw->GetSelected()->GetName()));

  tableWidget = new QTableWidget(this);  

  QPushButton *closeButton = new QPushButton(tr("Close"));
  QPushButton *saveButton = new QPushButton(tr("Save"));
  QPushButton *newButton = new QPushButton(tr("New"));

  popTable();

  connect(closeButton, SIGNAL(clicked()), this, SLOT(quit()));
  connect(saveButton, SIGNAL(clicked()), this, SLOT(save()));
  connect(newButton, SIGNAL(clicked()), this, SLOT(addRow()));

  QHBoxLayout *horizontalLayout = new QHBoxLayout;
  horizontalLayout->addWidget(tableWidget);

  QHBoxLayout *buttonsLayout = new QHBoxLayout;
  buttonsLayout->addStretch(1);
  buttonsLayout->addWidget(newButton);
  buttonsLayout->addWidget(saveButton,1);
  buttonsLayout->addWidget(closeButton,2);

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addLayout(horizontalLayout);
  mainLayout->addStretch(1);
  mainLayout->addSpacing(12);
  mainLayout->addLayout(buttonsLayout);
  setLayout(mainLayout);

  setWindowTitle(tr("Network Settings Dialog"));
}

void RoutingDialog::quit()
{
  emit close();
}

void RoutingDialog::save()
{
  size_t NRows = tableWidget->rowCount();

  for (size_t i = 0; i < m_rIndex.size(); i++)
  {
    std::string dest = tableWidget->item(i,0)->text().toStdString();
    std::string mask = tableWidget->item(i,1)->text().toStdString();
    std::string gate = tableWidget->item(i,2)->text().toStdString();
    size_t iface = tableWidget->item(i,3)->text().toInt();
    if (gate != "" && dest != "")
    {
      // TO DO:
      // check that each node has access to that gateway
      node->GetRoute(this->m_rIndex.at(i))->SetDestination(dest);
      node->GetRoute(this->m_rIndex.at(i))->SetGateway(gate);
      node->GetRoute(this->m_rIndex.at(i))->SetMask(mask);
    }
    else if (gate == "" && dest != "")
    {
      node->GetRoute(this->m_rIndex.at(i))->SetDestination(dest);
      node->GetRoute(this->m_rIndex.at(i))->SetMask(mask);
      node->GetRoute(this->m_rIndex.at(i))->SetGateway("");
      node->GetRoute(this->m_rIndex.at(i))->SetIFace(iface);
    }
    else
    {
      QMessageBox::about(this, "Routes", "Invalid route ignored");
    }
  }

  for (size_t i = m_rIndex.size(), j = 0; i < NRows; i++, j++)
  {
    std::string dest = tableWidget->item(i,0)->text().toStdString();
    std::string mask = tableWidget->item(i,1)->text().toStdString();
    std::string gate = tableWidget->item(i,2)->text().toStdString();
    size_t iface = tableWidget->item(i,3)->text().toInt();
    if (dest != "")
    {
      // check that all nodes that route is applied to have access to gateway and don't have L2 flag
      if (gate != "")
        node->AddRoute(dest, mask, gate, -1);
      else
        node->AddRoute(dest, mask, "", iface);
    }
  }
  QMessageBox::about(this, "Routes", "Route settings saved successfully");
  this->close();
}

void RoutingDialog::addRow()
{
  size_t row = tableWidget->rowCount();
  tableWidget->setRowCount(row+1);
  QTableWidgetItem *newItem;  
  newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter); 
  tableWidget->setItem(row, 0, newItem); 
  newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter); 
  tableWidget->setItem(row, 1, newItem); 
  newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter); 
  tableWidget->setItem(row, 2, newItem);
  newItem = new QTableWidgetItem(tr(""));
  newItem->setTextAlignment(Qt::AlignCenter); 
  tableWidget->setItem(row, 3, newItem);
}

void RoutingDialog::popTable()
{
  QTableWidgetItem *newItem;
  tableWidget->setMinimumWidth(370);
  //tableWidget->setMaximumWidth(219);
  tableWidget->setColumnCount(4);
  //tableWidget->setRowCount(1);
  tableWidget->horizontalHeader()->setStretchLastSection(true);
  //tableWidget->horizontalHeader()->setResizeMode(QHeaderView::Stretch); 
  QStringList labels;
  labels << "Destination" << "Mask" << "Gateway" << "Interface";
  tableWidget->setHorizontalHeaderLabels(labels);

  size_t NRoutes = node->GetNRoutes();
  for (size_t i = 0; i < NRoutes; i++) 
  {
    size_t row = tableWidget->rowCount();
    tableWidget->setRowCount(row+1);
    std::string dest = node->GetRoute(i)->GetDestination();
    std::string gate = node->GetRoute(i)->GetGateway();
    std::string mask = node->GetRoute(i)->GetMask();
    std::string iFace = utils::integerToString(node->GetRoute(i)->GetIFace());
    this->m_rIndex.push_back(i);

    if (iFace == "-1" || gate != "")
      iFace = "";

    newItem = new QTableWidgetItem(tr(dest.c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(row, 0, newItem); 
    newItem = new QTableWidgetItem(tr(mask.c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(row, 1, newItem); 
    newItem = new QTableWidgetItem(tr(gate.c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(row, 2, newItem); 
    newItem = new QTableWidgetItem(tr(iFace.c_str()));
    newItem->setTextAlignment(Qt::AlignCenter); 
    tableWidget->setItem(row, 3, newItem);
  }

}
